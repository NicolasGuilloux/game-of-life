#![allow(dead_code)]

extern crate rand;
extern crate game_of_life;

use std::{thread, time};
use game_of_life::table::*;

fn main()
{
    let wait_time = time::Duration::from_millis(50);
    let mut step: usize = 1;

    let mut table: Table = gen_table(50);
    let mut tmp_table: Table = table.clone();

    draw(&table, &step);
    thread::sleep(wait_time);

    loop {
        step += 1;
        compute_next_step(&mut table, &mut tmp_table);
        draw(&table, &step);
        thread::sleep(wait_time);
    }
}
