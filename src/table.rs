extern crate rand;
extern crate termion;

use std::mem;
use cell::*;

pub type Table = Vec<Vec<Cell>>;

// Generates a table filled with random cells
pub fn gen_table(size: usize) -> Table
{
    (0..size).map(|_| {
        (0..size).map(|_| {
            rand::random::<Cell>()
        }).collect()
    }).collect()
}

// Calculates the next step
pub fn compute_next_step(current_table: &mut Table, next_table: &mut Table)
{
    for x in 0..current_table.len() {
        for y in 0..current_table.len() {
            next_table[x][y] = get_next_state(current_table, x, y);
        }
    }

    mem::swap(current_table, next_table);
}

// Draws the table in the terminal
pub fn draw(table: &Table, step: &usize)
{
    let size = table.capacity();

    print!("{}{}", termion::clear::All, termion::cursor::Goto(1,1));

    for _ in 0..size {
        print!("──");
    }

    println!("───");

    for line in table {
        print!("|");

        for cell in line {
            print!("{}", cell);
        }

        println!(" |");
    }

    for _ in 0..size {
        print!("──");
    }

    println!("───");

    println!("Current step: {}", step);
}
