use rand::distributions::{Distribution, Standard};
use rand::Rng;
use std::fmt::{Display, Formatter, Result};
use std::cmp::{min, max};
use table::*;

#[derive(Clone)]
pub enum Cell {
    Dead,
    Alive
}

// Implement printing
impl Display for Cell
{
    fn fmt(&self, f: &mut Formatter) -> Result {
        let str = match self {
            Cell::Dead  => "  ",
            Cell::Alive => " ■",
        };

        write!(f, "{}", str)
    }
}

// Implement random
impl Distribution<Cell> for Standard
{
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Cell {
        if rng.gen() { Cell::Dead } else { Cell::Alive }
    }
}

// Get the next state of the cell based on its current state and its neighbours
pub fn get_next_state(table: &Table, cell_x: usize, cell_y: usize) -> Cell
{
    let current_state = &table[cell_x][cell_y];
    let cells_alive_around = get_alive_cells_around(table, cell_x, cell_y);

    match (current_state, cells_alive_around) {
        (Cell::Alive, 2) => Cell::Alive,
        (_, 3)           => Cell::Alive,
        _                => Cell::Dead
    }
}

// Count the number of cells alive in the neighbourhood
fn get_alive_cells_around(table: &Table, cell_x: usize, cell_y: usize)-> usize
{
    let mut sum = 0;
    let min_x = max(cell_x.saturating_sub(1), 0);
    let max_x = min(cell_x + 1, table.len() - 1);
    let min_y = max(cell_y.saturating_sub(1), 0);
    let max_y = min(cell_y + 1, table.len() - 1);

    for x in min_x..=max_x {
        for y in min_y..=max_y {
            if x != cell_x || y != cell_y {
                sum += match table[x][y] {
                    Cell::Alive => 1,
                    Cell::Dead  => 0
                };
            }
        }
    }

    sum
}
