extern crate rand;

pub mod cell;
pub mod table;

#[cfg(test)]
mod tests {
    use table::*;
    use cell::*;

    #[test]
    fn vertical_figure() {
        let state_one: Table = vec![
            vec![Cell:: Dead, Cell:: Dead, Cell:: Dead,  Cell:: Dead, Cell:: Dead],
            vec![Cell:: Dead, Cell:: Dead, Cell:: Alive, Cell:: Dead, Cell:: Dead],
            vec![Cell:: Dead, Cell:: Dead, Cell:: Alive, Cell:: Dead, Cell:: Dead],
            vec![Cell:: Dead, Cell:: Dead, Cell:: Alive, Cell:: Dead, Cell:: Dead],
            vec![Cell:: Dead, Cell:: Dead, Cell:: Dead,  Cell:: Dead, Cell:: Dead],
        ];

        let state_two: Table = vec![
            vec![Cell:: Dead, Cell:: Dead,  Cell:: Dead,  Cell:: Dead,  Cell:: Dead],
            vec![Cell:: Dead, Cell:: Dead,  Cell:: Dead,  Cell:: Dead,  Cell:: Dead],
            vec![Cell:: Dead, Cell:: Alive, Cell:: Alive, Cell:: Alive, Cell:: Dead],
            vec![Cell:: Dead, Cell:: Dead,  Cell:: Dead,  Cell:: Dead,  Cell:: Dead],
            vec![Cell:: Dead, Cell:: Dead,  Cell:: Dead,  Cell:: Dead,  Cell:: Dead],
        ];

        let mut current_state = state_one.clone();
        let mut tmp_state = state_one.clone();

        // assert_eq!(state_two, compute_next_step(current_state, tmp_table));
        // assert_eq!(state_one, compute_next_step(current_state, tmp_table));
    }
}
