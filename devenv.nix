{ pkgs, ... }:

{
  name = "Game of Life - Rust";
  languages.rust.enable = true;
  packages = with pkgs; [ 
    git 
  ] 
  # Darwin specific packages
  ++ (lib.optionals stdenv.isDarwin [
    darwin.apple_sdk.frameworks.Security
  ]);

  enterShell = ''
    echo "----- Game of Life - Rust -----"
  '';

  enterTest = ''
    echo "Building project..."
    pbuild
  '';

  scripts.pbuild.exec = "cargo build";
  scripts.prelease.exec = "cargo build --release";
}
